<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Request;
use GuzzleHttp;

class HomeController extends Controller
{
    public function productAvailabilities(Request $request)
    {
        $data = $this->getData();
        $filteredData = $this->filterData($data->product_availabilities);

        $this->exportFilteredDataToCSV($filteredData);

        return view('index');
    }

    public function getData()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://www.mocky.io/v2/58ff37f2110000070cf5ff16",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            throw new \Exception($err);
        } else {
            return json_decode($response);
        }
    }

    public function filterData($data)
    {
        $filteredData = [];
        foreach ($data as $product)
        {
            $activityStartDateTimeStart = strtotime('2017-10-30T11:59');
            $activityStartDateTimeEnd = strtotime('2017-11-31T11:59');
            $productActivityStartDatetime = strtotime($product->activity_start_datetime);

            $checkActivityStartDateTime = ($productActivityStartDatetime > $activityStartDateTimeStart && $productActivityStartDatetime < $activityStartDateTimeEnd) ? true : false;

            $checkPlacesAvailable = ($product->places_available >=1 && $product->places_available <= 30) ? true : false;

            if($checkPlacesAvailable && $checkActivityStartDateTime) {
                $filteredData[] = $product;
            }
        }

        return $filteredData;

    }

    /**
     * @param $data
     */
    public function exportFilteredDataToCSV($data)
    {
        $fp = fopen('export.csv', 'w');

        foreach ($data as $row) {
            fputcsv($fp, json_decode(json_encode($row), true), ";");
        }

        fclose($fp);
    }
}
