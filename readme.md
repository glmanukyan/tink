##Installation

1. Clone the project to your projects directory. Go to your projects directory and run ```$ git clone https://glmanukyan@bitbucket.org/glmanukyan/tink.git```
2. [Configure virtual host for the project](http://laravel-recipes.com/recipes/25/creating-an-apache-virtualhost)
3. Go to your project directory and run ```$ composer install```
4. Ready!

##About Project

By clicking the link (CLICK HERE TO FILTER AND EXPORT PRODUCT AVAILABILITIES) under Tink logo in the homepage, the system read the [API Endpoint](http://www.mocky.io/v2/58ff37f2110000070cf5ff16) and filters the information:
- Activity_start_datetime “2017-10-30T11:59 and 2017-11-31T11:59”
- Places_available between 1 - 30.

Then the system stores the filtered result in a csv file (export.csv — stored in the root of public folder) with ; column separator and redirects you the Product Availabilities page, where you can download the exported CSV file by clicking the download button.

The main functionality in written in one Laravel Controller, which you can check by this [link](https://bitbucket.org/glmanukyan/tink/src/217e4fa0f430f9741727f73b79e222ca95b18bdd/app/Http/Controllers/HomeController.php?at=master&fileviewer=file-view-default)

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](http://patreon.com/taylorotwell):

- **[Vehikl](http://vehikl.com)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Styde](https://styde.net)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
